<?php

// use App\Http\Controllers\ClientController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Client;



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('clients', 'ClientController@index'); //getALL
Route::get('clients', function(){
    $client=Client::all();
    return response()->json([
        'success' => true,
        'data' => $client,
    ]);
      });

Route::post('clients', function(Request $request){ $client = Client::create($request->all());

    return response()->json([
        'success' => true,
        'Message' => "Client sucessfully added .!",
    ]);

                                                });




Route::get('clients/{id}', function($id){$client= Client::find($id);
    return response()->json([
        'success' => true,
        'data' => $client,
    ]);
});

Route::delete('clients/{id}', function($id){
                                            $client =Client::find($id);
                                            $client->delete();
                                            return response()->json([
                                                'success' => true,
                                                'Message' => "Client sucessfully deleted .!",
                                            ]);
                                            });

 Route::put('clients/{id}', function(Request $request,$id){
    $client =Client::find($id);
    $client->CIN = $request->input('CIN');
    $client->Nom = $request->input('Nom');
    $client->save();
    return response()->json([
        'success' => true,
        'Message' => "Client sucessfully Updated .!",
    ]);
 });
